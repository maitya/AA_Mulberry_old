# Generated by Django 2.0.3 on 2018-03-31 17:03

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('Zomato', '0003_zomatorestaurant_cuisines'),
    ]

    operations = [
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_login', models.NullBooleanField()),
                ('city', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='Zomato.City')),
                ('cuisine', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='Zomato.Cuisine')),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
