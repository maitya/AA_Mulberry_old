from django.contrib import admin
from .models import FirstLogin, UserCuisine, UserDetail

admin.site.register(FirstLogin)
admin.site.register(UserDetail)
admin.site.register(UserCuisine)