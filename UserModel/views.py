from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from .apps import get_user_model
import json

# Create your views here.
@login_required
def model_view(request):
    user_model = get_user_model(request.user.id)

    cuisines = []
    weights = []
    for cuisine_obj in user_model["weights"].keys():
        cuisines.append(cuisine_obj.name)
        weights.append(user_model["weights"][cuisine_obj])

    data = {
        "cuisines": cuisines,
        "weights": weights
    }
    return render(request, 'view_model.html', {"data": data})