import datetime
from random import random
from django.apps import AppConfig
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Avg

from UserModel.models import UserDetail, UserCuisine
from Zomato.models import Cuisine


class UsermodelConfig(AppConfig):
    name = 'UserModel'


def get_user_model(user_id):
    """Get User Model and return as a dict."""
    try:
        user = User.objects.get(id=user_id)
        user_details = UserDetail.objects.get(user=user)
        user_cuisines_all = UserCuisine.objects.filter(user=user)
        user_cuisines_grouped = user_cuisines_all.\
            values("cuisine__name").annotate(avgWeight=Avg("weight"))

        weights = dict()
        for user_cuisine in user_cuisines_grouped:
            cuisine_obj = Cuisine.objects.get(name=user_cuisine["cuisine__name"])
            weights[cuisine_obj] = user_cuisine["avgWeight"]

        return dict(
            trylocal=user_details.try_local,
            reviewThreshold=user_details.rating,
            weights=weights
        )
    except ObjectDoesNotExist:
        pass


def create_cuisine_instance(user, cuisine_obj, weight):
    """Create an instance in User Cuisine model of User."""
    obj, _ = UserCuisine.objects.get_or_create(user=user,
                                      cuisine=cuisine_obj,
                                      weight=weight,
                                      updated=datetime.datetime.now())
    return obj.id


def delete_cuisine_instance(user, obj_id):
    """Delete an instance in User Cuisine model of User."""
    UserCuisine.objects.filter(id=obj_id, user=user).delete()
