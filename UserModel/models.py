from django.conf import settings
from django.db import models
from django.db.models.signals import post_save

from Zomato.models import City, Cuisine


class FirstLogin(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL,
                                on_delete=models.CASCADE)
    first_login = models.NullBooleanField()

    def __str__(self):
        return self.user.username

class UserDetail(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL,
                                on_delete=models.CASCADE)
    city = models.ForeignKey(City, on_delete=models.CASCADE)
    rating = models.IntegerField(default=0)
    try_local = models.BooleanField(default=False)

    def __str__(self):
        return self.user.username


class UserCuisine(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                                on_delete=models.CASCADE)
    cuisine = models.ForeignKey(Cuisine, on_delete=models.CASCADE)
    updated = models.DateTimeField()
    weight = models.FloatField()

    def __str__(self):
        return (self.user.username + "-" + \
                self.cuisine.name + "-" + self.updated.isoformat() + "-" +
                str(self.weight))



def post_save_user_receiver(sender, instance, created, *args, **kwargs):
    if created:
        FirstLogin.objects.get_or_create(user=instance, first_login=True)

post_save.connect(post_save_user_receiver,
                  sender=settings.AUTH_USER_MODEL)

