function draw(user_model) {

    console.log(user_model);

    var options = {
        scale: {
            ticks: {
                beginAtZero: true,
                max: 1,

            }
        }
    };

    var data = {
        labels: user_model["cuisines"],
        datasets: [{
            data: user_model["weights"]
        }]
    };

    var ctx = document.getElementById('myChart').getContext('2d');
    new Chart(ctx, {
        type: 'radar',
        data: data,
        options: options
    });
}