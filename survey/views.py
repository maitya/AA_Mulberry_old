import datetime

from django.views.decorators.csrf import csrf_protect
from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
from PastTravels.views import pastTravels
from UserModel.models import UserDetail, UserCuisine, FirstLogin
from Zomato.models import City, Cuisine
from Recommendation.cuisineLoader import Cuisines


def showSurvey(request):
    return render(request, 'survey.html', {})


@csrf_protect
def surveySubmitted(request):
    if request.method == "POST":
        data = request.POST
        try_local = data["bool"]
        rating = data["rating"]
        cuisines = data.getlist("cuisines[]")

        city = data["city"]
        if try_local == "true":
            try_local = True
        else:
            try_local = False

        city_obj = City.objects.get(name=city)
        UserDetail.objects.get_or_create(user=request.user,
                                         city=city_obj,
                                         rating=rating,
                                         try_local=try_local)

        weight = 1
        for cuisine in cuisines:
            cuisine_obj = Cuisine.objects.get(name=cuisine)
            UserCuisine.objects.get_or_create(user=request.user,
                                              cuisine=cuisine_obj,
                                              weight=weight,
                                              updated=datetime.datetime.now())

        firstlogin_obj, _ = FirstLogin.objects.get_or_create(
            user=request.user, defaults=dict(first_login=True))
        if firstlogin_obj.first_login:
            firstlogin_obj.first_login = False
            firstlogin_obj.save()

        return HttpResponse(status=204)
