# Generated by Django 2.0.3 on 2018-03-20 17:59

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('PastTravels', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='City',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=125, verbose_name='City Name')),
                ('zomato_id', models.IntegerField(verbose_name='Zomato city id')),
                ('country', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='PastTravels.WorldBorder')),
            ],
        ),
        migrations.CreateModel(
            name='CityCuisine',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('city', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='Zomato.City')),
            ],
        ),
        migrations.CreateModel(
            name='Cuisine',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=125, verbose_name='Cuisine Name')),
                ('zomato_id', models.IntegerField(verbose_name='Zomato cuisine id')),
            ],
        ),
        migrations.AddField(
            model_name='citycuisine',
            name='cuisine',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='Zomato.Cuisine'),
        ),
    ]
