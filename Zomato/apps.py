from django.apps import AppConfig


class ZomatoConfig(AppConfig):
    name = 'Zomato'
