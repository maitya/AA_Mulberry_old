import os

from django.core.management.base import BaseCommand

from Zomato.models import City, Cuisine, CityCuisine, ZomatoRestaurant
from PastTravels.models import WorldBorder
from Zomato.fetchZomatoContent import ZomatoContentFetcher
from django.contrib.gis.geos import Point
from Recommendation.cuisineLoader import Cuisines


class Command(BaseCommand):
    def handle(self, *args, **options):

        #clear the models before generating
        City.objects.all().delete()
        Cuisine.objects.all().delete()
        CityCuisine.objects.all().delete()
        ZomatoRestaurant.objects.all().delete()

        countries = WorldBorder.objects.all()

        country_name_to_obj = {}
        for c in countries:
            country_name_to_obj[c.name] = c

        fetcher = ZomatoContentFetcher()
        cuisine_list = Cuisines().getFinalCuisines()
        cuisine_id = []

        #this is to initialise the models for Cities and Cuisines
        cuisine_in_dublin = fetcher.getCuisines(fetcher.getIds()["Dublin"]);

        dublin, _ = City.objects.get_or_create(
            country=country_name_to_obj["Ireland"],
            name="Dublin",
            zomato_id=fetcher.getIds()["Dublin"]
        )

        london, _ = City.objects.get_or_create(
            country=country_name_to_obj["United Kingdom"],
            name="London",
            zomato_id=fetcher.getIds()["London"]
        )

        sydney, _ = City.objects.get_or_create(
            country=country_name_to_obj["Australia"],
            name="Sydney, NSW",
            zomato_id=fetcher.getIds()["Sydney, NSW"]
        )


        for key in cuisine_list:
            c,_ = Cuisine.objects.get_or_create(
                name=key,
                zomato_id = fetcher.cuisine_ids[key]
            )
            CityCuisine.objects.get_or_create(
                city=dublin,
                cuisine=c
            )
            CityCuisine.objects.get_or_create(
                city=london,
                cuisine=c
            )
            CityCuisine.objects.get_or_create(
                city=sydney,
                cuisine=c
            )
            print("created cuisine: ", str(c))
            cuisine_id.append(c.getId())

        #Load Restaurants by cities.
        #Dublin
        restaurants_in_dublin = fetcher.getRestaurantByCity(dublin.getId(), cuisine_id)
        for res in restaurants_in_dublin:
            res = res["restaurant"]
            print(res["name"] + " in " + str(dublin))
            point = Point(
                float(res["location"]["longitude"]),
                float(res["location"]["latitude"]),
                srid=res["id"]
            )
            obj,_ = ZomatoRestaurant.objects.get_or_create(
                city=dublin,
                name = res["name"],
                zomato_id = res["id"],
                address=res["location"]["address"],
                position = point,
                cuisines = res["cuisines"],
                thumb = res["thumb"],
                agg_rating = float(res["user_rating"]["aggregate_rating"]),
                votes = int(res["user_rating"]["votes"]),
                cost_for_two = int(res["average_cost_for_two"]),
                currency = res["currency"]
            )

        #London
        restaurants_in_london = fetcher.getRestaurantByCity(london.getId(), cuisine_id)
        for res in restaurants_in_london:
            res = res["restaurant"]
            print(res["name"] + " in " + str(london))
            point = Point(
                float(res["location"]["longitude"]),
                float(res["location"]["latitude"]),
                srid=res["id"]
            )

            obj,_ = ZomatoRestaurant.objects.get_or_create(
                city=london,
                name = res["name"],
                zomato_id = res["id"],
                address=res["location"]["address"],
                position = point,
                cuisines = res["cuisines"],
                thumb = res["thumb"],
                agg_rating = float(res["user_rating"]["aggregate_rating"]),
                votes = int(res["user_rating"]["votes"]),
                cost_for_two = int(res["average_cost_for_two"]),
                currency = res["currency"]

            )

        restaurants_in_sydney = fetcher.getRestaurantByCity(sydney.getId(), cuisine_id)
        for res in restaurants_in_sydney:
            res = res["restaurant"]
            print(res["name"] + " in " + str(sydney))
            point = Point(
                float(res["location"]["longitude"]),
                float(res["location"]["latitude"]),
                srid=res["id"]
            )

            obj,_ = ZomatoRestaurant.objects.get_or_create(
                city=sydney,
                name = res["name"],
                zomato_id = res["id"],
                address=res["location"]["address"],
                position = point,
                cuisines = res["cuisines"],
                thumb = res["thumb"],
                agg_rating = float(res["user_rating"]["aggregate_rating"]),
                votes = int(res["user_rating"]["votes"]),
                cost_for_two = int(res["average_cost_for_two"]),
                currency = res["currency"]

            )







