from django.core.management.base import BaseCommand
from Zomato.models import CountryToCuisineMap, WorldBorder, Cuisine
import os
import csv

class Command(BaseCommand):
    def handle(self, *args, **options):
        country2CuisinePath = os.path.abspath(os.path.join(
            os.path.dirname(__file__), '..', '..', 'data',
            'country2cuisine.csv'))
        with open(country2CuisinePath, "r") as csvFile:
            reader = csv.DictReader(csvFile)
            for row in reader:
                country = row["Country"]
                cuisine = row["Nationality"]

                if (WorldBorder.objects.filter(name=country).exists() and
                    Cuisine.objects.filter(name=cuisine).exists()):
                    print(country)
                    CountryToCuisineMap.objects.get_or_create(
                        country=WorldBorder.objects.get(name=country),
                        cuisine=Cuisine.objects.get(name=cuisine))