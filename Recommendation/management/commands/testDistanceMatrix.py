import os

from django.core.management.base import BaseCommand
from Recommendation.cuisineLoader import Cuisines
from Zomato.models import Cuisine

#testing the distance between different cuisines
class Command(BaseCommand):
    def handle(self, *args, **options):
        cuisineLoader = Cuisines()
        #print(cuisineLoader.getDistanceBetweenCuisines("Irish", "French",))
        obj = Cuisine.objects.get(name="Mexican")
        list = cuisineLoader.getClosestCuisines(obj)
        print(list)
