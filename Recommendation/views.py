import json

from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.http import JsonResponse, Http404
from django.shortcuts import render
from UserModel.apps import create_cuisine_instance, get_user_model,\
    delete_cuisine_instance
from Zomato.models import Cuisine, City, ZomatoRestaurant



# Create your views here.
@login_required
def search_view(request):
    return render(request, 'recommendation.html', {})


def dict_of_restaurant(resto, cuisine, origin):
    return dict(
        name=resto.name,
        address=resto.address,
        cuisines=cuisine.name,
        cuisine_id=cuisine.id,
        thumb=resto.thumb,
        rating=resto.agg_rating,
        nb_votes=resto.votes,
        cost_for_two=str(resto.cost_for_two) + resto.currency,
        origin=origin,
    )


def basic_search(user, city):
    # get all restaurants for one city
    restaurants = list(ZomatoRestaurant.objects.filter(city=city))

    # get cuisine
    resto_cuisine = {}
    for resto in restaurants:
        cuisines_str = [r.strip() for r in resto.cuisines.split(',')]
        for c in cuisines_str:
            try:
                c_obj = Cuisine.objects.get(name=c)
            except ObjectDoesNotExist:
                continue
            list_ = resto_cuisine.get(c_obj, set())
            list_.add(resto)
            resto_cuisine[c_obj] = set(list_)

    print(resto_cuisine.keys())

    model = get_user_model(user.id)

    ## FILTER RESULTS
    print("weights", model['weights'])
    weights = model['weights']
    total_weights = sum(weights.values())
    number_results = 20
    number_local = 0
    if model['trylocal']:
        number_results = 15
        number_local = 5
    list_cuisines = [(k, int(float(w) / total_weights * number_results))
                     for k, w in weights.items()]

    # 7 restaurants for the most liked cuisine
    #
    selected_restos = set()
    for cuisine, number in list_cuisines:
        for resto in list(resto_cuisine[cuisine])[:number]:
            selected_restos.add((resto, cuisine))
    other_cuisines = set(Cuisine.objects.all()) - set(k for k,
                                                            v in list_cuisines)

    other_restaurants = set()
    for cuisine in list(other_cuisines):
        for resto in list(resto_cuisine[cuisine]):
            other_restaurants.add((resto, cuisine))

    other_restaurants = list(other_restaurants)[:number_local]

    final_results = []
    for r, c in selected_restos:
        final_results.append(
            dict_of_restaurant(r, c, 'taste'))

    for r, c in other_restaurants:
        final_results.append(
            dict_of_restaurant(r, c, 'discover'))

    return final_results


@login_required
def get_results(request):
    if request.method == 'POST':
        data = json.loads(request.body.decode('utf-8'))
        city = City.objects.get(name=data['city'])

        restaurant_list =  basic_search(request.user, city)
        # save in session the restaurant list
        request.session['search_results'] = restaurant_list
        request.session['feedbacks'] = json.dumps({})

        return JsonResponse({'results': restaurant_list})
    return Http404("Page requires POST info")


positive_feedback = 1.0
negative_feedback = 0.
@login_required
def resto_feedback(request):
    if request.method == 'POST':
        data = json.loads(request.body.decode('utf-8'))
        restaurant_list = request.session['search_results']
        resto_id = data['resto-id']
        resto = restaurant_list[int(resto_id)]
        feedback_type = data['feedback']

        feedbacks = json.loads(request.session['feedbacks'])
        previous_feedback_id = feedbacks.get(resto_id, None)
        print("previous feedback id", previous_feedback_id)
        if previous_feedback_id is not None:
            delete_cuisine_instance(request.user, int(previous_feedback_id))

        #  neighbours = Cuisines.getClosestCuisines(cuisine) pass
        cuisine = Cuisine.objects.get(id=resto['cuisine_id'])
        if feedback_type == 1:
            exp_id = create_cuisine_instance(request.user, cuisine,
                                             positive_feedback)
            feedbacks[resto_id] = exp_id
        elif feedback_type == -1:
            exp_id = create_cuisine_instance(request.user, cuisine,
                                             negative_feedback)
            feedbacks[resto_id] = exp_id
        elif feedback_type == 0:
            # don't do anything
            pass

        print(cuisine.name, feedback_type)
        request.session['feedbacks'] = json.dumps(feedbacks)
        return JsonResponse({'response': True})
    return Http404("Page requires POST info")

